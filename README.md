# README #

This README would normally document whatever steps are necessary to get your application up and running.


##Set up

clone the repository into Intellij IDEA

##Excecution
Use TestRunner to execute the test and to generate the reports

### What is this repository for? ###
Mobiquity QA Assessment

1. API Automation
   Test scenarios has been mentioned in the feature file named MobiquityQA.feature,
   implementation for the same in available in MoqbiquityStep: src/test/java/StepDefinitions/MoqbiquityStep.java
   path for feature file : src/test/resources/features/MobiquityQA.feature
   

##Validations performed

1. Status code validation for each call
2. Schema validation for every response
3. headers validation for each call
4. Email format validation

## Scnearios Coverage 
There are two contexts which has been defined in the project as below:
1. Success context - Scenario is created to perform the overall happy path flow which has been asked to automate as per the assessment.
2. Error context - Two more scenarios are added to perform negative testing to check the basic behavior of the api.


## Coding Standards 

Reusable functions: Reusable functions has been defined in the "utils" package under "Utility.java" class. To decrease the 
complexity of the code.

Clean code: to make sure the code should look clean and will be easy to maintain , all the functions has been
            named meaningfully to avoid unnecessary comments within the code.


##Report
report for test execution has been present in report directory under the name Mobiquity.html.

##CircleCI
CICD has been implemented using CircleCI. Find below the link for the dashboard
https://app.circleci.com/pipelines/bitbucket/vatsabhi11/mobiquityqa?invite=true 

##Configuration
This directory has been created to declare variables globally

##Tools
API Automation : Rest Assured
Reporting: Extent Reports
Build Tool: Maven
CICD: CirlceCI
BDD: Cucumber
IDE: Intellij IDEA
Testing Framework: Junit
Language : Java



