package utils;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.MatcherAssert.assertThat;

public class Utility {

    public Properties properties;
    public RequestSpecification request = RestAssured.given();

    public Response response = null;

    public Utility() {


        if (!Config()) {
            try {

                throw new Exception("Configuration FIle is not loaded successfully");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public boolean Config() {
        properties = new Properties();
        String configPath = System.getProperty("user.dir") + "/Configuration/Environment.properties";

        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(configPath));
            properties = new Properties();
            try {
                properties.load(reader);
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Configuration.properties not found at " + configPath);
        }
        return true;

    }

    public String Property(String propertyName) {

        return properties.getProperty(propertyName);
    }

    public RequestSpecification setBaseURI() {


        return request.baseUri(Property("BaseURI"));

    }


    public RequestSpecification setQueryParam(String key, String value) {
        HashMap<String, String> queryParams = new HashMap<>();
        queryParams.put(key, value);
        return request.queryParams(queryParams);
    }

    public ArrayList<Integer> getJsonTextIntList(Response response, String value) {
        JsonPath jsonPathEvaluator = response.jsonPath();

        ArrayList<Integer> jsonIntegerValue = jsonPathEvaluator.get(value);

        return jsonIntegerValue;
    }


    public ArrayList<String> getJsonTextStr(Response response, String value) {
        JsonPath jsonPathEvaluator = response.jsonPath();

        ArrayList<String> jsonStringValue = jsonPathEvaluator.get(value);

        return jsonStringValue;
    }


    public void emailFormatChecker(String email) {
        String regex = "^[A-Za-z0-9+_.-]+@(.+)$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        if (matcher.matches())
            Assert.assertTrue(true);
        else
            Assert.assertTrue(false);

    }

    public void schemaValidator(Response res, String path) {
        String responseBody = res.getBody().asString();

        assertThat(responseBody, matchesJsonSchemaInClasspath(path));

    }
}
