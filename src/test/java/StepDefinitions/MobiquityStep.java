package StepDefinitions;

import com.aventstack.extentreports.ExtentReports;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import org.junit.Assert;
import utils.Utility;

public class MobiquityStep {

    public Response response = null;
    Utility util = new Utility();
    int counter = 0;

    @Given("User is initializing the api with the requested base URI")
    public void user_is_initializing_the_api_with_the_requested_base_uri() {

        util.setBaseURI();
    }

    @When("User added {string} to make a search based call")
    public void user_added_to_make_a_search_based_call(String paramValue) {


        util.setQueryParam(util.Property("user_param"), paramValue);

    }

    @When("User is searching the desired username")
    public void user_is_searching_the_desired_username() {


        response = util.request.get(util.Property("users_endpoint"));


    }

    @Then("User is validating the statuscode as {int}")
    public void user_is_validating_the_statuscode_as(Integer int1) {


        Assert.assertTrue(int1.equals(response.getStatusCode()));
    }

    @Then("User is validating the content type of the response")
    public void user_is_validating_the_content_type_of_the_response() {
        Assert.assertEquals("application/json; charset=utf-8", response.getContentType());

    }

    @Then("User is validating the user search response schema")
    public void user_is_validating_the_schema_of_the_response() {

        util.schemaValidator(response, "schemas/SearchUser_schema.json");


    }


    @Given("User is fetching the details of the posts written by the user")
    public void user_is_fetching_the_details_of_the_posts_written_by_the_user() {


        util.setBaseURI();


    }

    @Then("User is validating the posts response schema")
    public void user_is_validating_the_posts_response_schema() {

        util.schemaValidator(response, "schemas/posts_user_schema.json");
    }

    @Then("User is fetching the comments for each post from the user and performing the email and schema validation")
    public void user_is_fetching_the_comments_for_each_post_from_the_user() {


        for (Integer st : util.getJsonTextIntList(response, "id")) {

            util.setBaseURI();
            util.setQueryParam(util.Property("comments_ID"), st.toString());

            response = util.request.get(util.Property("comments_endpoint"));

            util.schemaValidator(response, "schemas/comments_user.json");

            util.emailFormatChecker(util.getJsonTextStr(response, "email").get(counter));

            counter++;
        }
    }

    @When("User is fetching the details from the previous call")
    public void user_is_fetching_the_details_from_the_previous_call() {


        util.setQueryParam(util.Property("id_param"), util.getJsonTextIntList(response, "id").get(0).toString());
    }

    @Then("User is searching the respective posts")
    public void user_is_searching_the_respective_posts() {


        response = util.request.get(util.Property("posts_endpoint"));
        response.prettyPrint();
    }


    @Then("User is verifying the empty body in the response")
    public void userIsVerifyingTheEmptyBodyInTheResponse() {


        Assert.assertTrue("Error context is validated with the empty response",response.getBody().asString().equals("[]"));
    }

    @And("User is searching the desired username with incorrect endpoint")
    public void userIsSearchingTheDesiredUsernameWithIncorrectEndpoint() {

        response = util.request.get(util.Property("incorrect_endpoint"));
    }

    @Given("User is initializing the posts api with the incorrect {string}")
    public void userIsInitializingThePostsApiWithTheIncorrect(String paramValue) {

        util.setBaseURI();
        util.setQueryParam(util.Property("id_param"), paramValue);

        response = util.request.get(util.Property("posts_endpoint"));
    }

    @Given("User is initializing the comments api with the incorrect {string}")
    public void userIsInitializingTheCommentsApiWithTheIncorrect(String paramValue) {

        util.setBaseURI();
        util.setQueryParam(util.Property("comments_ID"), paramValue);

        response = util.request.get(util.Property("comments_endpoint"));
    }
}
