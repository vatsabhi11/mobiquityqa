Feature:  Search for the user with username as specified

  @success
  Scenario Outline:  To test the success context of Search User Posts written by user and comments for each post

    Given User is initializing the api with the requested base URI
    When User added "<username>" to make a search based call
    And User is searching the desired username
    Then User is validating the statuscode as 200
    And User is validating the content type of the response
    Then User is validating the user search response schema
    Given User is fetching the details of the posts written by the user
    When User is fetching the details from the previous call
    Then User is searching the respective posts
    Then User is validating the statuscode as 200
    And User is validating the content type of the response
    And User is validating the posts response schema
    Then User is fetching the comments for each post from the user and performing the email and schema validation
    Then User is validating the statuscode as 200
    And User is validating the content type of the response

    Examples:
      | username |
      | Delphine |

  @error
  Scenario Outline: To test the error context for the search user using incorrect username
    Given User is initializing the api with the requested base URI
    When User added "<username>" to make a search based call
    And User is searching the desired username
    Then User is verifying the empty body in the response
    Examples:
      | username   |
      | DElhine123 |

  @error
  Scenario Outline: To test the error context for the posts api using incorrect userID
    Given User is initializing the posts api with the incorrect "<userId>"
    Then User is verifying the empty body in the response
    Examples:
      | userId |
      | abcs   |

  @error
  Scenario Outline: To test the error context for the comments api using incorrect id
    Given User is initializing the comments api with the incorrect "<id>"
    Then User is verifying the empty body in the response
    Examples:
      | id |
      | ab |


  @error
  Scenario Outline: To test the error context for the search user using incorrect endpoint
    Given User is initializing the api with the requested base URI
    When User added "<username>" to make a search based call
    And User is searching the desired username with incorrect endpoint
    Then User is validating the statuscode as 404
    Examples:
      | username |
      | Delphine |



